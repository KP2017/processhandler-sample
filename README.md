# Process Handler (Sample)

ProcessHandler is a wrapper over System.Diagnostic.Process which provide efficient callbacks for running any shell command asynchronously with realtime output.

Nuget dependency for [download](https://www.nuget.org/packages/ProcessHandler/)