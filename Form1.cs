﻿
/* This sample will demonstrate to use Process
 * asynchrously to run something on background
 * and then provide callbacks to update views.
 * 
 * Getting Started
 * ---------------
 * Import ProcessHandler.dll to your project
 */

using System;
using System.Windows.Forms;

/* Import this dependency. */
using kp.process;

namespace Sample
{
    public partial class Form1 : Form
    {
        private ProcessHandler process;
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This button click will demonstrate usage of all active callbacks
        /// </summary>
        private void button1_Click(object sender, EventArgs ex)
        {
            /* Create Process Options */
            ProcessOptions options = new ProcessOptions();
            options.FileName = @"cmd.exe";
            options.Arguments = $"/c help";

            /* Create new instance of ProcessHandler functions passing process options in constructor */
            process = new ProcessHandler(options);

            /* Subscribe to the process to recieve callbacks on this thread */
            process.Subscribe((output) =>
            {
                /* This block execute everytime when there is change in process output data (IO Thread) */

                /* Note
                 * ----
                 * Every callback you receieve in this block and error block are returned from IO thread,
                 * which means you cannot update views in Main thread.
                 * 
                 * For eg: textbox.text = "" will throw exception. Instead you've using my create UpdateTextAsync extension function.
                 * 
                 * Using this UpdateTextAsync function you can cleanly update the textbox value.
                 * Currently this function supports for Label, RichTextBox, TextBox only.
                 */

                textBox1.UpdateTextAsync($"{textBox1.Text}{Environment.NewLine}{output}");


                /* If you want to update views in other control use this below method (uncomment it)*/

                // button1.UpdateAsync(() =>
                // {
                //     button1.Text = "This changed!";
                // });

            }, (error) =>
            {
                /* This block execute everytime when there is change in process error data (IO Thread) */

                textBox1.UpdateTextAsync($"{textBox1.Text}{Environment.NewLine}Error: {error}");
                /// I am leaving it as blank for now....
            }, (o, e) =>
            {
                // This code will execute when process is complete (invokes on Main thread itself)
                MessageBox.Show("Process Complete");

                /* Also this EventArgs return some additional data like below */
                string outputDataComplete = e.OutputData;
                string errorDataComplete = e.ErrorData;
            });

            /* Finally start the process */
            process.Start();
        }

        /// <summary>
        /// This will demonstrate usage of only onComplete Callback
        /// </summary>
        private void button2_Click(object sender, EventArgs ex)
        {
            /* Create Process Options */
            ProcessOptions options = new ProcessOptions();
            options.FileName = @"cmd.exe";
            options.Arguments = $"/c help";

            /* Create new instance of ProcessHandler functions passing process options in constructor */
            process = new ProcessHandler(options);

            /* Subscribe to the process to recieve only complete callback on Main Thread */
            process.Subscribe((o, e) =>
            {
                // This code will execute when process is complete (invokes on Main thread itself)
                MessageBox.Show("Process Compelete");

                /* Also this EventArgs return some additional data like below */
                string outputDataComplete = e.OutputData;
                string errorDataComplete = e.ErrorData;
            });

            /* Start the process */
            process.Start();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (process!=null && process.IsRunning)
            { 
                process.Cancel();
            }
        }
    }
}
